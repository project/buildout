<?php

namespace Drupal\buildout;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\buildout\Exception\BuildoutException;

/**
 * Contains Batch API logic for Buildout synchronization.
 */
class BuildoutSyncBatch {

  /**
   * Translation function wrapper.
   *
   * @see \Drupal\Core\StringTranslation\TranslationInterface:translate()
   */
  public static function t($string, array $args = [], array $options = []) {
    return \Drupal::translation()->translate($string, $args, $options);
  }

  /**
   * Set message function wrapper.
   *
   * @see \Drupal\Core\Messenger\MessengerInterface
   */
  public static function message($message = NULL, $type = 'status', $repeat = TRUE) {
    \Drupal::messenger()->addMessage($message, $type, $repeat);
  }

  /**
   * Synchronization operation callback.
   *
   * @param bool $update
   *   Should existing data be updated?
   * @param mixed $context
   *   Batch context.
   */
  public static function doSync($update, &$context) {
    $integrator = \Drupal::service('buildout.property_integrator');

    // Initialize batch.
    if (!isset($context['sandbox']['offset'])) {
      $context['sandbox']['offset'] = 0;
      $context['results']['count'] = 0;
    }

    $batchSize = 20;

    try {
      $result = $integrator->getSyncProperties($context['sandbox']['offset'], $batchSize);
    }
    catch (BuildoutException $e) {
      static::message(static::t('Buildout API connection error: @error', [
        '@error' => $e->getMessage(),
      ]), 'error');
      return;
    }

    // \Drupal::logger('buildout')->notice(print_r($result, TRUE));
    // \Drupal::logger('buildout')->notice($result['count']);

    if (!isset($context['sandbox']['total'])) {
      $context['sandbox']['total'] = $result['count'];
    }

    if (!empty($result['properties'])) {
      $integrator->setUpdate($update);

      try {
        $data = $result['properties'];
        $integrator->syncProperties($data);

        // $context['sandbox']['offset']++;
        $context['sandbox']['offset'] = $context['sandbox']['offset'] + count($result['properties']);
        $context['finished'] = $context['sandbox']['offset'] / $context['sandbox']['total'];
        $context['message'] = static::t('Synchronized @count of @total properties.', [
          '@count' => $context['sandbox']['offset'],
          '@total' => $context['sandbox']['total'],
        ]);
        $context['results']['count'] = $context['results']['count'] + count($result['properties']);
      }
      catch (BuildoutException $e) {
        static::message(static::t('Buildout error: @error', [
          '@error' => $e->getMessage(),
        ]), 'error');
      }

    }

  }

  /**
   * Batch finished callback.
   *
   * @param bool $success
   *   Was the process successfull?
   * @param array $results
   *   Batch processing results.
   * @param array $operations
   *   Performed operations array.
   */
  public static function batchFinished($success, array $results, array $operations) {
    if ($success) {
      $message = static::t('Synchronized @count properties.', [
        '@count' => $results['count'],
      ]);
      $type = 'status';
    }
    else {
      $message = static::t('Finished with an error.');
      $type = 'error';
    }
    static::message($message, $type);
  }

  /**
   * Batch builder function.
   *
   * @param array $options
   *   Synchronization options passed to the batch operation.
   */
  public static function getBatch(array $options) {
    $current_class = get_called_class();

    $batchBuilder = (new BatchBuilder())
      ->setTitle('Synchronizing properties.')
      ->setFinishCallback([$current_class, 'batchFinished'])
      ->setProgressMessage('Synchronizing, estimated time left: @estimate, elapsed: @elapsed.')
      ->addOperation([$current_class, 'doSync'], $options);

    return $batchBuilder->toArray();
  }

}
