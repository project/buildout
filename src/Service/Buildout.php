<?php

namespace Drupal\buildout\Service;

use GuzzleHttp\ClientInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\buildout\Exception\BuildoutException;
use GuzzleHttp\Exception\ClientException;

/**
 * Defines the Buildout service class.
 */
class Buildout implements BuildoutInterface {

  const METHODS = [
    'getOffices' => [
      'path' => 'offices',
    ],
    'syncProperties' => [
      'path' => 'properties',
    ],
  ];

  /**
   * HTTP client object.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * Base URL for called methods.
   *
   * @var string
   */
  protected $baseUrl;

  /**
   * Service object constructor.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   HTTP client object.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory object.
   */
  public function __construct(
    ClientInterface $client,
    ConfigFactoryInterface $config_factory
  ) {
    $this->client = $client;

    $config = $config_factory->get('buildout.settings');
    $this->baseUrl = $config->get('api_base_url');
  }

  /**
   * Perform an API request.
   */
  protected function request($request_options) {
    $options = [];

    if (!empty($request_options['query'])) {
      $options['query'] = $request_options['query'];
    }
    if (!empty($request_options['body'])) {
      $options['body'] = json_encode($request_options['body']);
    }

    $uri = rtrim($this->baseUrl, '/') . '/' . $request_options['path'];

    // TODO: Add more error handling here with time and tests if required.
    try {
      $response = $this->client->request($request_options['method'], $uri, $options);

      if ($response->getStatusCode() === 200) {
        $output = json_decode($response->getBody()->getContents(), TRUE);
      }
      return $output;
    }
    catch (ClientException $e) {
      $output = json_decode($e->getResponse()->getBody()->getContents(), TRUE);
      $message = isset($output['error']['message']) ? $output['error']['message'] : 'Unknown error';
      throw new BuildoutException($message, $request_options);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function __call($method, $parameters) {
    if (!array_key_exists($method, self::METHODS)) {
      throw new BuildoutException('Unsupported method');
    }
    $request_options = self::METHODS[$method];
    $request_options += [
      'method' => 'GET',
    ];

    if (!empty($parameters)) {
      $parameters = $parameters[0];
      if (!is_array($parameters)) {
        if (!empty($parameters)) {
          $request_options['path'] .= '/' . $parameters;
        }
      }
      else {
        if (isset($parameters['method'])) {
          $request_options['method'] = $parameters['method'];
          unset($parameters['method']);
        }
        if ($request_options['method'] === 'GET') {
          $request_options['query'] = $parameters;
        }
        else {
          if (isset($parameters['body'])) {
            $request_options['body'] = $parameters['body'];
            unset($parameters['body']);
            $request_options['query'] = $parameters;
          }
          else {
            $request_options['body'] = $parameters;
          }
        }
      }
    }

    // \Drupal::logger('buildout')->notice(print_r($request_options, TRUE));

    return $this->request($request_options);
  }

}
