<?php

namespace Drupal\buildout\Service;

/**
 * Defines a Buildout property integration service.
 */
interface PropertyIntegratorInterface {

  /**
   * Sets connection info for the Buildout API service object.
   *
   * Use to override default values that are taken from
   * global configuration and are set when the API service is
   * initialized.
   *
   * @param array $data
   *   Connection data, values that take effect:
   *     - api_base_url,
   *     - api_key.
   */
  public function setConnectionInfo(array $data);

  /**
   * Sets the update parameter.
   *
   * @param bool $value
   *   The value. If set to TRUE, existing content that has
   *   been synchronized before will be updated.
   */
  public function setUpdate($value);

  /**
   * Performs a Buildout API request to get properties.
   *
   * @param int $offset
   *   The offset.
   * @param int $limit
   *   Max number of results.
   */
  public function getSyncProperties($offset, $limit);

  /**
   * Synchronizes a single property entity.
   *
   * @param array $data
   *   Property data from Buildout API.
   */
  public function syncProperty(array $data);

}
