<?php

namespace Drupal\buildout\Service;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Utility\Token;
use Drupal\buildout\Exception\BuildoutException;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\media\Entity\MediaType;

/**
 * Buildout property integration service implementation.
 */
class PropertyIntegrator implements PropertyIntegratorInterface {

  use StringTranslationTrait;

  /**
   * The buildout API service.
   *
   * @var \Drupal\buildout\Service\BuildoutInterface
   */
  protected $buildout;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Logger for this service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Drupal token service container.
   *
   * @var Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Should existing content be updated?
   *
   * @var bool
   */
  protected $update;

  /**
   * Constructor.
   *
   * @param \Drupal\buildout\Service\BuildoutInterface $buildout
   *   The Buildout API service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity Type Manager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   */
  public function __construct(
    BuildoutInterface $buildout,
    EntityTypeManagerInterface $entityTypeManager,
    FileSystemInterface $fileSystem,
    LoggerChannelFactoryInterface $logger_factory,
    EntityFieldManagerInterface $entity_field_manager,
    Token $token
  ) {
    $this->buildout = $buildout;
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
    $this->logger = $logger_factory->get('buildout');
    $this->entityFieldManager = $entity_field_manager;
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public function setConnectionInfo(array $data) {
    $this->buildout->setConnectionInfo($data);
  }

  /**
   * {@inheritdoc}
   */
  public function setUpdate($value) {
    $this->update = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getSyncProperties($offset, $limit) {
    return $this->buildout->syncProperties(['offset' => $offset, 'limit' => $limit]);
  }

  /**
   * {@inheritdoc}
   */
  public function syncProperty(array $data) {
    // \Drupal::logger('buildout')->notice(print_r($data, TRUE));
    $nodeStorage = $this->entityTypeManager->getStorage('node');
    $mediaStorage = $this->entityTypeManager->getStorage('media');

    // Check for an existing property with this ID.
    $properties = $nodeStorage->loadByProperties([
      'type' => 'property',
      'field_id' => $data['id'],
    ]);

    if (empty($data['name'])) {
      $data['name'] = $data['address'];
    }

    if (empty($properties)) {
      $node = $nodeStorage->create([
        'type' => 'property',
        'title' => $data['name'],
        'field_id' => $data['id'],
        'uid' => 1,
      ]);
      $node->save();
    }
    else {
      $node = reset($properties);
      if (!$this->update) {
        return;
      }
    }

    $node->title->value = $data['name'];
    $node->field_address->country_code = $data['country_code'];
    $node->field_address->address_line1 = $data['address'];
    $node->field_address->administrative_area = $data['state'];
    $node->field_address->locality = $data['city'];
    $node->field_address->postal_code = $data['zip'];

    if (!empty($data['latitude']) && !empty($data['longitude'])) {
      $node->field_location->lat = $data['latitude'];
      $node->field_location->lng = $data['longitude'];
    }

    if (!empty($data['sale_price_dollars'])) {
      $node->field_price = $data['sale_price_dollars'];
    }

    $originalImages = [];
    if (!$node->get('field_images')->isEmpty()) {
      foreach ($node->get('field_images')->getValue() as $value) {
        $originalImages[] = $value['target_id'];
      }
    }

    $media = [];
    if (!empty($data['photos'])) {
      foreach ($data['photos'] as $photo) {
        if (isset($photo['type']) && $photo['type'] === 'Property Photo') {
          $mediaId = $this->syncPhoto($photo, $data['id']);
          if (!empty($mediaId)) {
            $media[] = $mediaId;
            unset($originalImages[$mediaId]);
          }
        }
      }
    }

    $mediaToDelete = $mediaStorage->loadMultiple($originalImages);
    $mediaStorage->delete($mediaToDelete);

    $node->field_images = $media;
    $node->save();

  }

  /**
   * {@inheritdoc}
   */
  public function syncProperties(array $data) {
    foreach ($data as $record) {
      $this->syncProperty($record);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function syncPhoto(array $data, int $propertyId) {
    // \Drupal::logger('buildout')->notice(print_r($data, TRUE));

    $fileContents = file_get_contents($data['original_file_url']);
    if ($fileContents === FALSE) {
      return FALSE;
    }

    $urlinfo = parse_url($data['original_file_url']);
    $fileinfo = pathinfo($urlinfo['path']);
    $filename = $propertyId . '-' . $data['id'] . '.' . strtolower($fileinfo['extension']);

    $fileStorage = $this->entityTypeManager->getStorage('file');

    // Check for an existing file entity with the same filename.
    $files = $fileStorage->loadByProperties([
      'filename' => $filename,
    ]);

    $fieldDefinitions = $this->entityFieldManager->getFieldDefinitions('media', 'image');
    $fieldStorageDefinitions = $this->entityFieldManager->getFieldStorageDefinitions('media');
    $file_directory = $fieldDefinitions['field_media_image']->getSetting('file_directory');
    $scheme = $fieldStorageDefinitions['field_media_image']->getSetting('uri_scheme');

    // Replace tokens in file directory string.
    $file_directory = $this->token->replace($file_directory);

    $file_directory = $scheme . '://' . $file_directory;

    if (!$this->fileSystem->prepareDirectory($file_directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      $this->logger->warning('Could not prepare destination directory @dir for media.', [
        '@dir' => $file_directory,
      ]);
      return NULL;
    }

    $file = NULL;
    if (!empty($files)) {
      $file = reset($files);
      // Replace the existing file.
      $this->fileSystem->saveData($fileContents, $file->getFileUri(), FileSystemInterface::EXISTS_REPLACE);
    }
    else {
      $file = file_save_data($fileContents, $file_directory . '/' . $filename, FileSystemInterface::EXISTS_REPLACE);
    }

    $mediaStorage = $this->entityTypeManager->getStorage('media');

    $mediaEntities = $mediaStorage->loadByProperties([
      'field_media_image' => $file->id(),
    ]);

    $media = NULL;
    if (!empty($mediaEntities)) {
      $media = reset($mediaEntities);
    }
    else {
      $media = $mediaStorage->create([
        'bundle' => 'image',
        'uid' => 1,
        'field_media_image' => $file->id(),
      ]);
      $media->save();
    }

    return $media->id();
  }

}
