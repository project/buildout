<?php

namespace Drupal\buildout\Commands;

use Consolidation\AnnotatedCommand\AnnotationData;
use Drupal\buildout\Service\BuildoutInterface;
use Drupal\buildout\BuildoutSyncBatch;
use Drupal\buildout\Exception\BuildoutException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class BuildoutCommands extends DrushCommands {

  /**
   * Drupal\buildout\Service\BuildoutInterface definition.
   *
   * @var \Drupal\buildout\Service\BuildoutInterface
   */
  protected $buildout;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    BuildoutInterface $buildout,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory
  ) {
    $this->buildout = $buildout;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * Test the Buildout API connection.
   *
   * @command buildout:test
   *
   * @usage drush buildout:test
   * @aliases bt,buildout-test
   */
  public function test() {

    try {
      $office_data = $this->buildout->getOffices();
      $this->output()->writeln(dt(print_r($office_data, TRUE)));
    }
    catch (BuildoutException $e) {
      dt('<error>' . $e->getMessage() . '</error>');
    }

  }

  /**
   * Sync Buildout properties to Drupal property nodes.
   *
   * @param bool $update
   *   A boolean, update existing synced properties.
   *
   * @command buildout:sync-properties
   * @usage drush buildout:sync-properties
   * @aliases bsp,buildout-sync-properties
   */
  public function syncProperties($update) {

    $batch = BuildoutSyncBatch::getBatch([
      'update' => $update,
    ]);
    batch_set($batch);
    $this->output()->writeln(dt('<info>Starting sync...</info>'));
    drush_backend_batch_process();

  }

}
