<?php

namespace Drupal\buildout\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\buildout\BuildoutSyncBatch;

/**
 * Defines the Buildout synchronization form.
 */
class BuildoutSynchronizationForm extends FormBase {

  /**
   * Constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'buildout_synchronization_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

/*
    $stores_options = [];
    foreach ($this->printfulStores as $store_id => $store) {
      $stores_options[$store_id] = $store->get('label');
    }

    $form['printful_store_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Printful store to be synchronized'),
      '#options' => $stores_options,
      '#required' => TRUE,
    ];
*/

    $form['update'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update existing data'),
      '#default_value' => FALSE,
      '#description' => $this->t('If checked, existing properties will be updated, otherwise only new items will be imported.'),
    ];

    $form['execute_sync'] = [
      '#type' => 'submit',
      '#value' => $this->t('Execute'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    batch_set(BuildoutSyncBatch::getBatch([
      'update' => $form_state->getValue('update'),
    ]));
  }

}
