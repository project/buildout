<?php

namespace Drupal\buildout\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The Buildout configuration form.
 */
class BuildoutConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'buildout_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('buildout.settings');

    $form['connection'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Connection'),
    ];

    $form['connection']['api_base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Buildout API base URL'),
      '#default_value' => $config->get('api_base_url'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('buildout.settings');

    // Save connection config values.
    $config->set('api_base_url', $values['api_base_url']);
    $config->save();
    $this->messenger()->addStatus($this->t('Buildout configuration updated.'));
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'buildout.settings',
    ];
  }

}
